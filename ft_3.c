/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_3.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/09 18:06:54 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 17:21:32 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void						ft_x3(t_mlx *a, t_dataimg *s, t_xy m)
{
	long double				x;

	x = (long double)a->ofs;
	while (x < (a->ofs) + 1920)
	{
		m.x = x - (long double)a->ofs;
		m.cr = x / a->scc;
		ft_depth_draw3(s, a, m);
		x = x + 1;
	}
}

void						*ft_y3(void *v)
{
	long double				y;
	t_xy					m;
	t_th					*data;

	data = v;
	y = (long double)data->a->ofsy + 135 * data->inter;
	while (y < data->a->ofsy + (135 * (data->inter + 1)))
	{
		m.ci = y / data->a->scc;
		m.y = y - data->a->ofsy;
		ft_x3(data->a, data->s, m);
		y = y + 1;
	}
	data = NULL;
	return (NULL);
}

void						ft_xy3(t_mlx *a, t_dataimg *s)
{
	t_th					*data;
	t_th					*sdata;
	int						i;
	pthread_t				*threads;

	if ((threads = (pthread_t *)malloc((8 * sizeof(pthread_t)))) == NULL)
		ft_memory_error(a, s, NULL);
	i = 0;
	if ((data = malloc(sizeof(t_th) * 8)) == NULL)
		ft_memory_error(a, s, threads);
	sdata = data;
	while (i < 8)
	{
		sdata->a = a;
		sdata->s = s;
		sdata->inter = i;
		pthread_create(&threads[i++], NULL, ft_y3, (void *)sdata);
		sdata++;
	}
	while (i >= 0)
		pthread_join(threads[i--], NULL);
	free(data);
	free(threads);
}

void						ft_3(t_mlx *a, long double d, long double scc)
{
	t_dataimg				*s;

	s = malloc(sizeof(t_dataimg));
	if (s == NULL)
		ft_exit_x(a);
	s->data = mlx_get_data_addr(a->img, &(s->bpp), &(s->siz), &(s->end));
	a->scale = d;
	a->scc = scc;
	ft_xy3(a, s);
	free(s);
}

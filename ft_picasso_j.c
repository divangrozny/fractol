/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_picasso_j.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 17:17:11 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 18:20:14 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int							ft_sccj(int key, t_mlx *x)
{
	mlx_clear_window(x->mlx_ptr, x->win_ptr);
	if (key == 12)
	{
		if (x->scc <= 200000000000000000)
		{
			x->ofs = x->ofs + x->ofs;
			x->ofsy = x->ofsy + x->ofsy;
			ft_julia(x, x->scale + 2, x->scc * (long double)2);
		}
		else
			ft_julia(x, x->scale, x->scc);
	}
	if (key == 14)
	{
		if (x->scc >= 2)
		{
			x->ofs = x->ofs / 2;
			x->ofsy = x->ofsy / 2;
			ft_julia(x, x->scale - 2, x->scc / 2);
		}
		else
			ft_julia(x, x->scale, x->scc);
	}
	mlx_put_image_to_window(x->mlx_ptr, x->win_ptr, x->img, 0, 0);
	return (0);
}

int							ft_movej(int key, t_mlx *x)
{
	mlx_clear_window(x->mlx_ptr, x->win_ptr);
	if (key == 123)
		x->ofs = x->ofs - 100;
	if (key == 124)
		x->ofs = x->ofs + 100;
	if (key == 125)
		x->ofsy = x->ofsy + 100;
	if (key == 126)
		x->ofsy = x->ofsy - 100;
	ft_julia(x, x->scale, x->scc);
	mlx_put_image_to_window(x->mlx_ptr, x->win_ptr, x->img, 0, 0);
	return (0);
}

int							ft_keyj(int key, t_mlx *x)
{
	if (key == 53)
		ft_exit_x(x);
	if (key == 78 || key == 69)
		ft_redj(key, x);
	if (key == 123 || key == 124 || key == 125 || key == 126)
		ft_movej(key, x);
	if (key == 12 || key == 14)
		ft_sccj(key, x);
	return (0);
}

int							ft_mouse_j(int b, int x, int y, t_mlx *a)
{
	if (b == 4)
		ft_keyj(12, a);
	if (b == 5)
		ft_keyj(14, a);
	x = 0;
	y = 0;
	return (0);
}

void						ft_picasso_j(int a)
{
	t_mlx					*pic;

	a = 0;
	if ((pic = malloc(sizeof(t_mlx))) == NULL)
		exit(0);
	pic->mlx_ptr = mlx_init();
	pic->win_ptr = mlx_new_window(pic->mlx_ptr, 1920, 1080, "mlx 42");
	pic->ofs = 0;
	pic->ofsy = 0;
	pic->scc = 50;
	pic->cre = 0.311;
	pic->cim = -0.4842;
	pic->img = mlx_new_image(pic->mlx_ptr, 1920, 1080);
	ft_julia(pic, 30, 70);
	mlx_put_image_to_window(pic->mlx_ptr, pic->win_ptr, pic->img, 0, 0);
	mlx_key_hook(pic->win_ptr, ft_keyj, pic);
	mlx_hook(pic->win_ptr, 6, 0, ft_mouse, pic);
	mlx_mouse_hook(pic->win_ptr, ft_mouse_j, pic);
	mlx_hook(pic->win_ptr, 17, 1L << 17, ft_exit_x, pic);
	mlx_loop(pic->mlx_ptr);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/11 18:16:34 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 18:17:59 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int					ft_move3(int key, t_mlx *x)
{
	mlx_clear_window(x->mlx_ptr, x->win_ptr);
	if (key == 123)
		x->ofs = x->ofs - 280;
	if (key == 124)
		x->ofs = x->ofs + 280;
	if (key == 125)
		x->ofsy = x->ofsy + 280;
	if (key == 126)
		x->ofsy = x->ofsy - 280;
	ft_3(x, x->scale, x->scc);
	mlx_put_image_to_window(x->mlx_ptr, x->win_ptr, x->img, 0, 0);
	return (0);
}

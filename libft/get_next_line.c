/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/06 16:57:57 by odale-dr          #+#    #+#             */
/*   Updated: 2019/02/23 18:59:26 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int						ft_check(const char *str)
{
	int							i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] == '\n')
			return (i);
		i++;
	}
	return (-1);
}

static char						*ft_reading(const int fd, t_gnl *upr)
{
	int							bytes;
	char						buffer[BUFF_SIZE + 1];
	char						*temp;

	bytes = 0;
	temp = NULL;
	bytes = read(fd, buffer, BUFF_SIZE);
	if (bytes == 0)
		upr->flag = -1;
	buffer[bytes] = '\0';
	if ((temp = ft_strdup(buffer)) == NULL)
		return (NULL);
	return (temp);
}

static char						*ft_upr_s(const int fd, char *buf, t_gnl *xxx)
{
	char						*x;
	char						*y;
	char						*z;

	if (buf == NULL)
		if ((buf = ft_strnew(0)) == NULL)
			return (NULL);
	if ((y = ft_strdup(buf)) == NULL)
		return (NULL);
	ft_strdel(&buf);
	xxx->flag = 1;
	while (ft_check(y) == -1 && xxx->flag != -1)
	{
		if ((x = ft_reading(fd, xxx)) == NULL)
			return (NULL);
		if ((z = ft_strjoin(y, x)) == NULL)
			return (NULL);
		ft_strdel(&y);
		ft_strdel(&x);
		y = z;
		z = NULL;
	}
	return (y);
}

static t_gnl					*ft_perebor(const int fd)
{
	static t_gnl				*upravlenie;
	t_gnl						*vr;

	vr = upravlenie;
	while (vr != NULL)
	{
		if (vr->desc == fd)
			return (vr);
		if (vr->next == NULL)
		{
			vr->next = malloc(sizeof(t_gnl));
			vr->next->desc = fd;
			vr->next->buff = NULL;
			vr->next->next = NULL;
			vr->next->flag = 1;
		}
		vr = vr->next;
	}
	upravlenie = malloc(sizeof(t_gnl));
	upravlenie->desc = fd;
	upravlenie->buff = NULL;
	upravlenie->next = NULL;
	upravlenie->flag = 1;
	return (ft_perebor(fd));
}

int								get_next_line(const int fd, char **line)
{
	t_gnl					*x;
	char					*change;

	if (fd < 0 || line == NULL || BUFF_SIZE < 1 || (read(fd, NULL, 0) == -1))
		return (-1);
	x = ft_perebor(fd);
	if ((change = ft_upr_s(fd, x->buff, x)) == NULL)
		return (-1);
	if (ft_check(change) == -1)
	{
		x->buff = ft_strnew(0);
		if (ft_strlen(change) == 0)
			return (0);
		if ((*line = ft_strdup(change)) == NULL)
			return (-1);
		ft_strdel(&change);
		return (1);
	}
	change[ft_check(change)] = '\0';
	x->buff = ft_strdup(change + ft_strlen(change) + 1);
	if ((*line = ft_strdup(change)) == NULL)
		return (-1);
	ft_strdel(&change);
	return (1);
}

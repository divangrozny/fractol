/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/26 20:43:28 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/10 14:41:06 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnew(size_t size)
{
	char	*n;

	n = (char *)malloc(size + 1);
	if (n == NULL)
		return (NULL);
	ft_bzero(n, size);
	n[size] = '\0';
	return (n);
}

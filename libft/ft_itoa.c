/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/04 19:40:52 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/06 18:29:30 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char			*ft_itneg(int n, char *s, size_t k)
{
	s[0] = '-';
	while (n < -9)
	{
		s[k] = (-1) * (n % 10) + '0';
		n = n / 10;
		k--;
	}
	s[k] = '0' + (n * (-1));
	return (s);
}

static char			*ft_itpos(int n, char *s, size_t k)
{
	while (n > 9)
	{
		s[k] = n % 10 + '0';
		n = n / 10;
		k--;
	}
	s[k] = '0' + n;
	return (s);
}

static size_t		ft_size(int n)
{
	size_t			is;
	int				c;

	is = 1;
	c = 10;
	if (n < 0)
		is++;
	if (n == -2147483648)
		return (11);
	if (n >= -9 && n <= 9)
		return (is);
	while (n / c > 9 || n / c < -9)
	{
		c = c * 10;
		is++;
	}
	return (is + 1);
}

char				*ft_itoa(int n)
{
	char			*sum;
	size_t			kol;

	kol = ft_size(n);
	sum = ft_strnew(kol);
	if (sum == NULL)
		return (NULL);
	if (n < 0)
		return (ft_itneg(n, sum, kol - 1));
	return (ft_itpos(n, sum, kol - 1));
}

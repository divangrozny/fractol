/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 20:31:11 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/10 14:52:09 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strchr(const char *s, int c)
{
	char	*ss;
	char	cc;

	ss = (char *)s;
	cc = (char)c;
	while (*ss)
	{
		if (*ss == cc)
		{
			return (ss);
		}
		ss++;
	}
	if (cc == '\0')
	{
		return (ss);
	}
	return (NULL);
}

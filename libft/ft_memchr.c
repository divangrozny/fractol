/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/15 20:25:54 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/10 14:43:57 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*src;
	unsigned char	kc;

	src = (unsigned char *)s;
	kc = (unsigned char)c;
	while (n != 0)
	{
		if (*src == kc)
			return ((void *)src);
		src++;
		n--;
	}
	return (NULL);
}

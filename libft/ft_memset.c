/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/15 15:54:58 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/10 14:45:49 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void					*ft_memset(void *b, int c, size_t len)
{
	size_t				i;
	unsigned char		*k;

	i = 0;
	k = (unsigned char *)b;
	while (i < len)
	{
		k[i] = c;
		i++;
	}
	return (b);
}

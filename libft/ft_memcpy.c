/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/15 16:58:15 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/10 14:44:08 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memcpy(void *dst, const void *src, size_t n)
{
	char	*mdst;
	char	*msrc;
	size_t	i;

	mdst = (char *)dst;
	msrc = (char *)src;
	i = 0;
	while (i < n)
	{
		mdst[i] = msrc[i];
		i++;
	}
	return (dst);
}

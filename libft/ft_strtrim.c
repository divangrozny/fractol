/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/04 16:55:02 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/06 18:34:11 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_whiten(char const *s)
{
	size_t		x;

	x = 0;
	while (s[x] == ' ' || s[x] == '\n' || s[x] == '\t')
	{
		x++;
	}
	return (x);
}

static size_t	ft_whitek(char const *s)
{
	size_t		kx;
	size_t		xs;

	kx = 0;
	xs = ft_strlen(s);
	while (s[xs - 1] == ' ' || s[xs - 1] == '\n' || s[xs - 1] == '\t')
	{
		xs--;
		kx++;
	}
	return (kx);
}

static int		ft_fullz(char const *s)
{
	size_t		fi;

	fi = 0;
	while (s[fi] == ' ' || s[fi] == '\n' || s[fi] == '\t')
	{
		fi++;
	}
	if (s[fi] == '\0')
		return (0);
	return (1);
}

char			*ft_strtrim(char const *s)
{
	size_t		i;
	size_t		j;
	char		*new;

	i = 0;
	j = 0;
	if (s == NULL)
		return (NULL);
	new = ft_strnew(ft_strlen(s) - ft_whiten(s) - (ft_fullz(s) * ft_whitek(s)));
	if (new == NULL)
		return (NULL);
	while (s[i] != '\0' && ft_fullz(s) == 1)
	{
		if (i >= ft_whiten(s) && i < (ft_strlen(s) - ft_whitek(s)))
		{
			new[j] = s[i];
			j++;
		}
		i++;
	}
	new[j] = '\0';
	return (new);
}

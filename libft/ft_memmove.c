/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/15 18:53:41 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/06 18:28:46 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void			*ft_memdst(char *mdst, const char *msrc, size_t mlen)
{
	while (mlen > 0)
	{
		mdst[mlen - 1] = msrc[mlen - 1];
		mlen--;
	}
	mdst[mlen] = msrc[mlen];
	return (mdst);
}

void				*ft_memmove(void *dst, const void *src, size_t len)
{
	const char		*mmsrc;
	char			*mmdst;
	size_t			i;

	mmdst = (char *)dst;
	mmsrc = (const char *)src;
	i = 0;
	if (len == 0)
		return (dst);
	if (mmsrc < mmdst)
	{
		mmdst = ft_memdst(mmdst, mmsrc, len);
	}
	else
	{
		while (i < len)
		{
			mmdst[i] = mmsrc[i];
			i++;
		}
	}
	return (dst);
}

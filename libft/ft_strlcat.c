/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 18:45:04 by odale-dr          #+#    #+#             */
/*   Updated: 2018/12/18 20:29:01 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	fin;
	size_t	i;

	fin = 0;
	i = 0;
	while (dst[fin] != '\0' && fin < size)
	{
		fin++;
	}
	if (fin == size)
	{
		return (fin + ft_strlen(src));
	}
	while (src[i] != '\0' && (fin + i) < (size - 1))
	{
		dst[fin + i] = src[i];
		i++;
	}
	dst[fin + i] = '\0';
	return (fin + ft_strlen(src));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 21:06:19 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/10 15:07:15 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strstr(const char *haystack, const char *needle)
{
	char	*x;
	char	*telo;
	int		i;
	int		j;

	i = 0;
	x = (char *)needle;
	telo = (char *)haystack;
	if (x[i] == '\0')
		return (telo);
	while (telo[i] != '\0')
	{
		j = 0;
		while (telo[i + j] == x[j])
		{
			if (x[j + 1] == '\0')
				return (telo + i);
			j++;
		}
		i++;
	}
	return (NULL);
}

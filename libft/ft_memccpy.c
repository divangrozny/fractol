/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/15 17:31:48 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/06 18:13:46 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t			i;
	unsigned char	*mdst;
	unsigned char	*msrc;

	i = 0;
	mdst = (unsigned char *)dst;
	msrc = (unsigned char *)src;
	while (i < n)
	{
		mdst[i] = msrc[i];
		if (msrc[i] == (unsigned char)c && i < n)
			return ((void *)(dst + i + 1));
		i++;
	}
	return (NULL);
}

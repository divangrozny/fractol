/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/27 20:22:46 by odale-dr          #+#    #+#             */
/*   Updated: 2018/12/27 20:59:37 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	size_t			i;
	size_t			r;

	r = 0;
	if (s != NULL && f != NULL)
	{
		i = ft_strlen(s);
		while (r != i)
		{
			(f)((unsigned int)r, &s[r]);
			r++;
		}
	}
}

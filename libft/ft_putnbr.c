/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/05 17:31:47 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/06 18:30:04 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		ft_pcif(int cn)
{
	if (cn > 0)
		ft_putchar('0' + cn);
	if (cn < 0)
	{
		ft_putchar('-');
		ft_putchar('0' + (cn * (-1)));
	}
}

static void		ft_ppos(int pn)
{
	int			c;

	c = 10;
	while (pn > 9)
	{
		while (pn / c > 9)
		{
			c = c * 10;
		}
		ft_putchar('0' + (pn / (c)));
		pn = pn - ((pn / c) * c);
		c = c / 10;
	}
	while (c != 1)
	{
		ft_putchar('0' + 0);
		c = c / 10;
	}
	ft_putchar('0' + (pn));
}

static void		ft_pneg(int nn)
{
	int			zamena;

	zamena = 0;
	if (nn == -2147483648)
	{
		ft_putchar('-');
		ft_putchar('2');
		ft_ppos(147483648);
	}
	if (nn != -2147483648)
	{
		ft_putchar('-');
		zamena = nn * (-1);
		ft_ppos(zamena);
	}
}

void			ft_putnbr(int n)
{
	if (n == 0)
		ft_putchar('0');
	if (n <= 9 && n >= -9 && n != 0)
		ft_pcif(n);
	if (n < -9)
		ft_pneg(n);
	if (n > 9)
		ft_ppos(n);
}

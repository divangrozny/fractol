/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/20 20:45:06 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/10 15:01:51 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t		i;
	int			j;

	i = 0;
	j = 0;
	if (n == 0)
		return (j);
	while ((unsigned char)s1[i] == (unsigned char)s2[i] && s1[i] != '\0')
	{
		if (i++ == n)
			return (j);
	}
	if (i < n)
	{
		return ((int)((unsigned char)s1[i] - (unsigned char)s2[i]));
	}
	return (0);
}

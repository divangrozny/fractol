/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/06 15:14:06 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/10 14:49:52 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		ft_pcif_fd(int cn, int fd)
{
	if (cn > 0)
		ft_putchar_fd('0' + cn, fd);
	if (cn < 0)
	{
		ft_putchar_fd('-', fd);
		ft_putchar_fd('0' + (cn * (-1)), fd);
	}
}

static void		ft_ppos_fd(int pn, int fd)
{
	int			c;

	c = 10;
	while (pn > 9)
	{
		while (pn / c > 9)
		{
			c = c * 10;
		}
		ft_putchar_fd('0' + (pn / (c)), fd);
		pn = pn - ((pn / c) * c);
		c = c / 10;
	}
	while (c != 1)
	{
		ft_putchar_fd('0' + 0, fd);
		c = c / 10;
	}
	ft_putchar_fd('0' + (pn), fd);
}

static void		ft_pneg_fd(int nn, int fd)
{
	int			zamena;

	zamena = 0;
	if (nn == -2147483648)
	{
		ft_putchar_fd('-', fd);
		ft_putchar_fd('2', fd);
		ft_ppos_fd(147483648, fd);
	}
	if (nn != -2147483648)
	{
		ft_putchar_fd('-', fd);
		zamena = nn * (-1);
		ft_ppos_fd(zamena, fd);
	}
}

void			ft_putnbr_fd(int n, int fd)
{
	if (fd < 0)
		return ;
	if (n == 0)
		ft_putchar_fd('0', fd);
	if (n <= 9 && n >= -9 && n != 0)
		ft_pcif_fd(n, fd);
	if (n < -9)
		ft_pneg_fd(n, fd);
	if (n > 9)
		ft_ppos_fd(n, fd);
}

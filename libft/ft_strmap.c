/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/27 21:03:19 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/10 15:00:37 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strmap(char const *s, char (*f)(char))
{
	char	*res;
	size_t	i;

	i = 0;
	if (s == NULL)
		return (NULL);
	res = ft_strnew(ft_strlen(s));
	if (res == 0)
		return (NULL);
	while (s[i] != '\0')
	{
		res[i] = (f)(s[i]);
		i++;
	}
	return (res);
}

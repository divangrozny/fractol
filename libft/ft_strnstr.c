/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 19:09:42 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/10 15:04:00 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	char	*x;
	char	*telo;
	size_t	i;
	size_t	j;

	i = 0;
	x = (char *)needle;
	telo = (char *)haystack;
	if (x[i] == '\0')
		return (telo);
	if (ft_strlen(x) > ft_strlen(telo) || ft_strlen(x) > len || len == 0)
		return (NULL);
	while (i <= (len - ft_strlen(x)))
	{
		j = 0;
		while (x[j] == telo[i + j])
		{
			if (x[j + 1] == '\0' || x[j] == '\0')
				return (telo + i);
			j++;
		}
		i++;
	}
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/20 21:20:25 by odale-dr          #+#    #+#             */
/*   Updated: 2019/01/06 18:28:23 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_c(char c)
{
	if (c >= 48 && c <= 57)
		return (1);
	return (0);
}

static int		ft_p(const char *p, int pp)
{
	if (pp == 0)
		return (1);
	if (pp == 1 && p[0] == '-')
		return (1);
	if (p[pp - 1] == '-' || p[pp - 1] == '+')
		pp = pp - 1;
	pp--;
	while (pp != -1 && ((p[pp] >= 9 && p[pp] <= 13) || p[pp] == 32))
	{
		pp--;
	}
	if (pp == -1)
	{
		return (1);
	}
	return (0);
}

static int		ft_usl(const char *x)
{
	int			u;

	u = 0;
	while (x[u] != 0 && ft_c(x[u]) != 1)
	{
		u++;
	}
	if (ft_c(x[u]) == 1)
		return (u);
	return (-1);
}

static int		ft_zn(const char *zstr, int zn)
{
	if (zn > 0 && '-' == zstr[ft_usl(zstr) - 1])
		return ((-1) * (-48 + ((int)zstr[zn])));
	return (-48 + ((int)zstr[zn]));
}

int				ft_atoi(const char *str)
{
	int			i;
	int			zn;

	zn = 0;
	i = ft_usl(str);
	if (i == -1 || ft_p(str, ft_usl(str)) == 0)
		return (0);
	while (ft_c(str[i]) == 1)
	{
		zn = zn * 10 + ft_zn(str, i);
		i++;
	}
	return (zn);
}

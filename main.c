/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/22 16:41:28 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 17:32:21 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int				ft_choose(const char *a)
{
	if ((a[0] == 'M' || a[0] == 'm') && a[1] == '\0')
		return (1);
	if ((a[0] == 'J' || a[0] == 'j') && a[1] == '\0')
		return (2);
	if ((a[0] == 'S' || a[0] == 's') && a[1] == '\0')
		return (3);
	if (ft_strcmp(a, "Mandelbrot") == 0 || ft_strcmp(a, "mandelbrot") == 0)
		return (1);
	if (ft_strcmp(a, "Julia") == 0 || ft_strcmp(a, "julia") == 0)
		return (2);
	if (ft_strcmp(a, "Spider") == 0 || ft_strcmp(a, "spider") == 0)
		return (3);
	ft_putstr("\n		Please, select (write whole word or first letter) ");
	ft_putstr("the fractal:\n		-");
	ft_putstr("--> Mandelbrot\n		---> Julia\n		---> Spider\n\n");
	ft_putstr("		For examle:  ./fractol M\n");
	return (0);
}

int				main(int argc, char **argv)
{
	int			f;

	f = 0;
	if (argc != 2)
	{
		ft_putstr("\n		");
		ft_putstr("Please, select (write whole word or first letter)");
		ft_putstr(" the fractal:\n		-");
		ft_putstr("--> Mandelbrot\n		---> Julia\n		---> Spider\n\n");
		ft_putstr("		For examle:  ./fractol M\n");
		exit(0);
	}
	if ((f = ft_choose(argv[1])) == 0)
		exit(0);
	if (f == 1)
		ft_picasso(1);
	if (f == 2)
		ft_picasso_j(2);
	if (f == 3)
		ft_picasso_3(3);
	return (0);
}

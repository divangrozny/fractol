/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memory_error.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/11 17:12:11 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 18:24:06 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void				ft_memory_error(t_mlx *a, t_dataimg *s, pthread_t *th)
{
	ft_putstr("\nMemory error\n");
	free(s);
	if (th != NULL)
		free(th);
	ft_exit_x(a);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_scc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/11 18:00:52 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 18:01:30 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int					ft_scc(int key, t_mlx *x)
{
	mlx_clear_window(x->mlx_ptr, x->win_ptr);
	if (key == 12)
	{
		if (x->scc <= 2000000000000000000)
		{
			x->ofs = x->ofs - (-960 - x->ofs);
			x->ofsy = x->ofsy - (-540 - x->ofsy);
			ft_mald(x, x->scale + 3, x->scc * (long double)2);
		}
		else
			mlx_put_image_to_window(x->mlx_ptr, x->win_ptr, x->img, 0, 0);
	}
	if (key == 14)
	{
		if (x->scc >= 25)
		{
			x->ofs = (x->ofs - 960) / 2;
			x->ofsy = (x->ofsy - 540) / 2;
			ft_mald(x, x->scale - 3, x->scc / 2);
		}
		else
			mlx_put_image_to_window(x->mlx_ptr, x->win_ptr, x->img, 0, 0);
	}
	mlx_put_image_to_window(x->mlx_ptr, x->win_ptr, x->img, 0, 0);
	return (0);
}

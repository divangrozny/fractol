/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_depth_draw.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/11 16:41:24 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 16:46:46 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void				ft_pixel_put_to_image(t_mlx *e, t_xy m, t_dataimg *s, int q)
{
	uint			r;
	uint			g;
	uint			b;
	int				x;
	int				y;

	y = (int)m.y;
	x = (int)m.x;
	if (y < 0 || y > 1080 || x < 0 || x > 1920)
		return ;
	b = mlx_get_color_value(e->mlx_ptr, (q & 0xff));
	g = mlx_get_color_value(e->mlx_ptr, (q & 0xff00) >> 8);
	r = mlx_get_color_value(e->mlx_ptr, (q & 0xff0000) >> 16);
	*(s->data + y * s->siz + (s->bpp / 8) * x) = b;
	*(s->data + y * s->siz + (s->bpp / 8) * x + 1) = g;
	*(s->data + y * s->siz + (s->bpp / 8) * x + 2) = r;
}

uint				ft_col(t_mlx *a, double t)
{
	long double		q;
	long double		r;

	q = a->scale;
	r = q / t;
	if (q - t <= 2)
		return (4259845);
	if (q - t <= 4)
		return (6488064);
	else
		return (t * 10);
}

void				ft_depth_draw(t_dataimg *s, t_mlx *a, t_xy m)
{
	long double		xn;
	long double		yn;
	long double		xn1;
	long double		yn1;
	int				d;

	xn1 = 0;
	yn1 = 0;
	d = 0;
	while (d < a->scale)
	{
		xn = xn1;
		yn = yn1;
		xn1 = (xn * xn) - (yn * yn) + m.cr;
		yn1 = 2 * xn * yn + m.ci;
		d++;
		if ((xn1 * xn1) + (yn1 * yn1) > 4)
			break ;
	}
	if (d < a->scale)
		ft_pixel_put_to_image(a->mlx_ptr, m, s, ft_col(a, (double)d));
	else
	{
		ft_pixel_put_to_image(a->mlx_ptr, m, s, (uint)0007);
	}
}

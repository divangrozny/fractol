/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/22 16:38:30 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 18:17:58 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H
# include "libft/libft.h"
# include <unistd.h>
# include <string.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <math.h>
# include "minilibx_macos/mlx.h"
# include <pthread.h>

typedef struct		s_mlx{
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img;
	long double		scale;
	long double		scc;
	long double		ofs;
	long double		ofsy;
	long double		cre;
	long double		cim;
}					t_mlx;

typedef struct		s_dataimg{
	char			*data;
	int				bpp;
	int				siz;
	int				end;
}					t_dataimg;

typedef struct		s_m{
	t_dataimg		s;
	t_mlx			*a;
	double			ci;
	double			cr;
}					t_m;

typedef struct		s_xy{
	long double		x;
	long double		y;
	long double		d;
	double			ci;
	double			cr;
}					t_xy;

typedef struct		s_th{
	t_mlx			*a;
	t_dataimg		*s;
	t_xy			xy;
	int				inter;
}					t_th;

typedef struct		s_j{
	long double		newre;
	long double		newim;
	long double		oldre;
	long double		oldim;
	long double		x;
	long double		y;
}					t_j;

int					main(int argc, char **argv);
void				ft_picasso(int a);
void				ft_mald(t_mlx *a, long double d, long double scc);
void				ft_picasso_j(int a);
void				ft_julia(t_mlx *a, long double d, long double scc);
int					ft_exit_x(t_mlx *x);
void				ft_picasso_3(int a);
void				ft_3(t_mlx *a, long double d, long double scc);
void				ft_depth_draw3(t_dataimg *s, t_mlx *a, t_xy m);
void				ft_depth_j(t_dataimg *s, t_mlx *a, t_j m);
void				ft_depth_draw(t_dataimg *s, t_mlx *a, t_xy m);
int					ft_mouse(int x, int y, t_mlx *a);
void				ft_memory_error(t_mlx *a, t_dataimg *s, pthread_t *th);
int					ft_scc(int key, t_mlx *x);
int					ft_redj(int key, t_mlx *x);
int					ft_move3(int key, t_mlx *x);

#endif

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/02/23 15:19:15 by odale-dr          #+#    #+#              #
#    Updated: 2019/05/11 18:17:57 by odale-dr         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol
SRC = main.c ft_picasso.c ft_mald.c ft_picasso_j.c ft_julia.c ft_exit_x.c ft_3.c ft_picasso_3.c ft_depth_draw3.c ft_depth_j.c ft_depth_draw.c ft_mouse.c ft_memory_error.c ft_scc.c ft_redj.c ft_move3.c
FILESO = ${SRC:.c=.o}
FLAGS = -Wall -Wextra -Werror
LIBA = -L ./libft -lft
MIN = -L ./minilibx_macos -lmlx
ADD = -framework OpenGL -framework AppKit
INCL = -I ./libft
INCLM = -I ./minilibx_macos

all: $(NAME)

$(NAME): $(FILESO)
	make -C ./libft/
	make -C ./minilibx_macos/
	gcc $(FLAGS) -o $(NAME) $(SRC) $(LIBA) $(MIN) $(ADD)
clean:
	rm -f $(FILESO)
	make clean -C ./libft/
	make clean -C ./minilibx_macos/

fclean: clean
	rm -f $(NAME)
	make fclean -C ./libft/

re: fclean all

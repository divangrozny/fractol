/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_depth_j.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/11 16:12:45 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 17:34:20 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void						ft_pixel_putj(t_mlx *e, t_j m, t_dataimg *s, int c)
{
	uint					r;
	uint					g;
	uint					b;
	int						x;
	int						y;

	y = (int)m.y;
	x = (int)m.x;
	if (y < 0 || y > 1080 || x < 0 || x > 1920)
		return ;
	r = mlx_get_color_value(e->mlx_ptr, (c & 0xff));
	g = mlx_get_color_value(e->mlx_ptr, (c & 0xff00) >> 8);
	b = mlx_get_color_value(e->mlx_ptr, (c & 0xff0000) >> 16);
	*(s->data + y * s->siz + (s->bpp / 8) * x) = b;
	*(s->data + y * s->siz + (s->bpp / 8) * x + 1) = g;
	*(s->data + y * s->siz + (s->bpp / 8) * x + 2) = r;
}

uint						ft_colj(t_mlx *a, double t)
{
	long double				q;
	long double				r;

	q = a->scale;
	r = q / t;
	if (q - t <= (q / 30) * 8)
		return (1000 + t);
	else
		return (222 + 15 * t);
}

void						ft_depth_j(t_dataimg *s, t_mlx *a, t_j m)
{
	long double				oldre;
	long double				oldim;
	long double				curr_it;

	curr_it = 0;
	while (curr_it < a->scale)
	{
		oldre = m.newre;
		oldim = m.newim;
		m.newre = oldre * oldre - oldim * oldim + a->cre;
		m.newim = 2 * oldre * oldim + a->cim;
		if (m.newre * m.newre + m.newim * m.newim > 4)
			break ;
		curr_it++;
	}
	if (curr_it < a->scale)
		ft_pixel_putj(a->mlx_ptr, m, s, ft_colj(a, curr_it));
	else
	{
		ft_pixel_putj(a->mlx_ptr, m, s, (uint)8888888);
	}
}

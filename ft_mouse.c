/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mouse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/11 17:02:08 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 17:34:54 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int						ft_mouse(int x, int y, t_mlx *a)
{
	long double			cx;
	long double			cy;

	cx = (long double)x;
	cy = (long double)y;
	cx = cx / 4000;
	cy = (cy - 560) / 1080;
	mlx_clear_window(a->mlx_ptr, a->win_ptr);
	a->cre = cx + 0.2;
	a->cim = cy;
	ft_julia(a, a->scale, a->scc);
	mlx_put_image_to_window(a->mlx_ptr, a->win_ptr, a->img, 0, 0);
	return (0);
}

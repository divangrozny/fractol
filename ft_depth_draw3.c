/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_depth_draw3.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/11 16:07:31 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 16:09:00 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void						ft_pixel_put3(t_mlx *e, t_xy m, t_dataimg *s, int q)
{
	uint					r;
	uint					g;
	uint					b;
	int						x;
	int						y;

	y = (int)m.y;
	x = (int)m.x;
	if (y < 0 || y > 1080 || x < 0 || x > 1920)
		return ;
	g = mlx_get_color_value(e->mlx_ptr, (q & 0xff));
	b = mlx_get_color_value(e->mlx_ptr, (q & 0xff00) >> 8);
	r = mlx_get_color_value(e->mlx_ptr, (q & 0xff0000) >> 16);
	*(s->data + y * s->siz + (s->bpp / 8) * x) = b;
	*(s->data + y * s->siz + (s->bpp / 8) * x + 1) = g;
	*(s->data + y * s->siz + (s->bpp / 8) * x + 2) = r;
}

uint						ft_col3(t_mlx *a, double t)
{
	long double				q;
	long double				r;

	q = a->scale;
	r = q / t;
	if (q - t <= (q / 25) * 2)
		return (1664012);
	if (q - t <= (q / 25) * 4)
		return (2319372);
	if (q - t <= (q / 25) * 8)
		return (2974732);
	if (q - t <= (q / 25) * 16)
		return (3630092);
	if (q - t <= (q / 25) * 18)
		return (4613132);
	else
		return (2319372 + t * 10);
}

void						ft_depth_draw3(t_dataimg *s, t_mlx *a, t_xy m)
{
	long double				tx;
	long double				zx;
	long double				ty;
	long double				zy;
	int						d;

	zx = m.cr;
	zy = m.ci;
	d = 0;
	while (d < a->scale && (zx * zx) + (zy * zy) < 40)
	{
		tx = zx;
		ty = zy;
		zx = (tx * tx) - (ty * ty) + m.cr;
		zy = 2 * tx * ty + m.ci;
		m.cr = m.cr / 2 + zx;
		m.ci = m.ci / 2 + zy;
		d++;
	}
	if (d < a->scale)
		ft_pixel_put3(a->mlx_ptr, m, s, ft_col3(a, (double)d));
	else
	{
		ft_pixel_put3(a->mlx_ptr, m, s, (uint)16777215);
	}
}

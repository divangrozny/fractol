/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_julia.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 17:15:03 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 17:20:48 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void					ft_jy(t_mlx *a, t_dataimg *s, t_j m)
{
	long double			y;

	y = (long double)a->ofsy;
	while (y < (a->ofsy) + 1080)
	{
		m.newim = (y - 1080 / 2) / (a->scc * 1080 / 200);
		m.y = y - a->ofsy;
		ft_depth_j(s, a, m);
		y = y + 1;
	}
}

void					*ft_jx(void *v)
{
	long double			x;
	t_j					m;
	t_th				*data;

	data = v;
	x = data->a->ofs + 120 * data->inter;
	while (x < data->a->ofs + 120 * (data->inter + 1))
	{
		m.newre = 1.5 * (x - 1920 / 2) / (data->a->scc * 1920 / 200);
		m.x = x - data->a->ofs;
		ft_jy(data->a, data->s, m);
		x = x + 1;
	}
	data = NULL;
	return (NULL);
}

void					ft_xyj(t_mlx *a, t_dataimg *s)
{
	t_th				*data;
	t_th				*sdata;
	int					i;
	pthread_t			*threads;

	if ((threads = (pthread_t *)malloc((16 * sizeof(pthread_t)))) == NULL)
		ft_memory_error(a, s, NULL);
	i = 0;
	if ((data = malloc(sizeof(t_th) * 16)) == NULL)
		ft_memory_error(a, s, threads);
	sdata = data;
	while (i < 16)
	{
		sdata->a = a;
		sdata->s = s;
		sdata->inter = i;
		pthread_create(&threads[i++], NULL, ft_jx, (void *)sdata);
		sdata++;
	}
	while (i >= 0)
		pthread_join(threads[i--], NULL);
	free(data);
	free(threads);
}

void					ft_julia(t_mlx *a, long double d, long double scc)
{
	t_dataimg			*s;

	s = malloc(sizeof(t_dataimg));
	if (s == NULL)
		ft_exit_x(a);
	s->data = mlx_get_data_addr(a->img, &(s->bpp), &(s->siz), &(s->end));
	a->scale = d;
	a->scc = scc;
	ft_xyj(a, s);
	free(s);
}

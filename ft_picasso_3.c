/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_picasso_3.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/09 16:52:01 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 18:18:13 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int					ft_red3(int key, t_mlx *x)
{
	int				c;
	long double		scc;

	scc = x->scc;
	c = x->scale;
	if (key == 78)
	{
		if (c <= 50)
			c = c - 5;
		else
			c = c - 10;
		if (c < 0)
			c = 0;
	}
	if (key == 69)
		c = c + 10;
	ft_3(x, c, scc);
	mlx_clear_window(x->mlx_ptr, x->win_ptr);
	mlx_put_image_to_window(x->mlx_ptr, x->win_ptr, x->img, 0, 0);
	return (0);
}

int					ft_scc3(int key, t_mlx *x)
{
	mlx_clear_window(x->mlx_ptr, x->win_ptr);
	if (key == 12)
	{
		if (x->scc <= 2000000000000000000)
		{
			x->ofs = x->ofs - (-960 - x->ofs);
			x->ofsy = x->ofsy - (-540 - x->ofsy);
			ft_3(x, x->scale + 3, x->scc * (long double)2);
		}
		else
			mlx_put_image_to_window(x->mlx_ptr, x->win_ptr, x->img, 0, 0);
	}
	if (key == 14)
	{
		if (x->scc >= 25)
		{
			x->ofs = (x->ofs - 960) / 2;
			x->ofsy = (x->ofsy - 540) / 2;
			ft_3(x, x->scale - 3, x->scc / 2);
		}
		else
			mlx_put_image_to_window(x->mlx_ptr, x->win_ptr, x->img, 0, 0);
	}
	mlx_put_image_to_window(x->mlx_ptr, x->win_ptr, x->img, 0, 0);
	return (0);
}

int					ft_key3(int key, t_mlx *x)
{
	if (key == 53)
		ft_exit_x(x);
	if (key == 78 || key == 69)
		ft_red3(key, x);
	if (key == 123 || key == 124 || key == 125 || key == 126)
		ft_move3(key, x);
	if (key == 12 || key == 14)
		ft_scc3(key, x);
	return (0);
}

int					ft_mouse_s(int b, int x, int y, t_mlx *a)
{
	if (b == 4)
		ft_key3(12, a);
	if (b == 5)
		ft_key3(14, a);
	x = 0;
	y = 0;
	return (0);
}

void				ft_picasso_3(int a)
{
	t_mlx			*pic;

	a = 0;
	if ((pic = malloc(sizeof(t_mlx))) == NULL)
		exit(0);
	pic->mlx_ptr = mlx_init();
	pic->win_ptr = mlx_new_window(pic->mlx_ptr, 1920, 1080, "mlx 42");
	pic->ofs = -960;
	pic->ofsy = -540;
	pic->scc = -540;
	pic->img = mlx_new_image(pic->mlx_ptr, 1920, 1080);
	ft_3(pic, 24, 400);
	mlx_put_image_to_window(pic->mlx_ptr, pic->win_ptr, pic->img, 0, 0);
	mlx_key_hook(pic->win_ptr, ft_key3, pic);
	mlx_mouse_hook(pic->win_ptr, ft_mouse_s, pic);
	mlx_hook(pic->win_ptr, 17, 1L << 17, ft_exit_x, pic);
	mlx_loop(pic->mlx_ptr);
}

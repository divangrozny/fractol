/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_redj.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: odale-dr <odale-dr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/11 18:09:28 by odale-dr          #+#    #+#             */
/*   Updated: 2019/05/11 18:10:25 by odale-dr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int							ft_redj(int key, t_mlx *x)
{
	int						c;
	long double				scc;

	scc = x->scc;
	c = x->scale;
	if (key == 78)
	{
		if (c <= 50)
			c = c - 10;
		else
			c = c - 25;
		if (c < 0)
			c = 0;
	}
	if (key == 69)
		c = c + 20;
	ft_julia(x, c, scc);
	mlx_clear_window(x->mlx_ptr, x->win_ptr);
	mlx_put_image_to_window(x->mlx_ptr, x->win_ptr, x->img, 0, 0);
	return (0);
}
